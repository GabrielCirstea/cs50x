#include <stdio.h>
#include "cs50.h"

int main()
{
	string s = get_string("Nume: ");
	printf("Ai scris %s\n", s);

	char c = get_char("si un simplu caracter: ");
	printf("Caracter: %c\n", c);
	
	int n = get_int("Un int: ");
	printf("Introdus: %d\n", n);

	float f = get_float("Un float: ");
	printf("Introdus: %f\n", f);

	return 0;
}
