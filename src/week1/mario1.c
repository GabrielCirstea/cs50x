#include <stdio.h>
#include "cs50.h"

int main()
{
	int n;
	do{
		n = get_int("Height: ");
	}while(n < 1);

	int i;
	for(i = 0; i < n; ++i){
		int j;
		for(j=0; j < n-(i+1); ++j){
			printf(" ");
		}
		for(j=n-(i+1); j < n; ++j){
			printf("#");
		}
		printf("\n");
	}
}
