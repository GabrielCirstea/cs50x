/* readability.c */

#include <stdio.h>
#include <ctype.h>
#include <cs50.h>
#include <string.h>

int main()
{
	string text = get_string("Text");

	int words = 0, letters = 0, sentences = 0;

	int i;
	for(i=0; text[i]; ++i) {
		if (isalnum(text[i])){
			letters++;
		} else if(isblank(text[i])){
			words++;
		} else if (strchr(".!?", text[i])) {
			sentences++;
		}
	}
	words++;

	// printf("W: %d, L: %d, S: %d\n", words, letters, sentences);
	float L = (float)letters * (100.0/words);
	float S = (float)sentences * (100.0/words);
	// printf("L: %f, S: %f\n", L, S);
	float grade = 0.0588 * L - 0.296 * S - 15.8;
	// printf("Grade: %f\n", grade);

	if (grade < 1) {
		printf("Before Grade 1\n");
		return 0;
	}

	int f_grade = grade;
	if (grade-f_grade > 0.5)
		grade = f_grade+1;
	else
		grade = f_grade;
	if (grade > 16)
		printf("Grade 16+\n");
	else
		printf("Grade %d\n", (int)grade);

	return 0;
}
