/* Aplly a simple substitution chiper
 * The key is provided in command line arguments
 * The plain text will be asked from stdin
 * The chiper text will be displayd on stdout*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "cs50.h"

int key_valid(string s);
char chip(char c, string key);

int main(int argc, char *argv[])
{
	if(argc != 2){
		printf("Usage: %s key\n", argv[0]);
		return 1;
	}
	int i;

	if(!key_valid(argv[1])){
		printf("Key must contain 26 characters.\n");
		return 1;
	}

	string plain = get_string("plaintest:  ");

	for(i=0; plain[i]; i++){
		plain[i] = chip(plain[i], argv[1]);
	}

	printf("ciphertext: %s\n", plain);

	return 0;
}

/* apply the cipher
 * - get the letter, remember if it was upper
 * - get the index of the leter on the key
 * - get the coresponding letter from the key
 * - it it was upper make it upper again
 * - because we don't care about the letter from the key, will make the leter lower
 * first*/
char chip(char c, string key)
{
	// remember if it was upper
	int to_upper = 0;
	if(isupper(c))
		to_upper = 1;
	c = tolower(c);

	// no letter
	if(!islower(c))
		return c;

	// get the index in key
	c -= 'a';

	// get te coresponding letter from key and make it lower
	c = tolower(key[c]);
	if(to_upper)
		c = toupper(c);

	return c;
}

/* validate the key
 * - use a 26 chars array to chef if every letter is provided only once
 * and if there are 26 letters
 * - check if there are only letters
 *   return 1 if valid and 0 otherwise*/
int key_valid(string s)
{
	char key[26] = {0};

	int i;
	// count every letter
	for(i = 0; s[i]; i++){
		char c;
		c = tolower(s[i]);
		// if it was not a letter
		if(!islower(c))
			return 0;
		c -= 'a';
		key[c]++;
	}

	for(i = 0; i < 26; i++)
		if(key[i] != 1){
			// printf("litera %c nu e %d - %d\n", i + 'a', i, key[i]);
			return 0;
		}

	return 1;
}
