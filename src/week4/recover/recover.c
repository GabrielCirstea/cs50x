#include <stdio.h>
#include <stdlib.h>

#define BUF_SIZE 512

int is_jpg_header(u_int8_t bufer[])
{
	if(bufer[0] != 0xff)
		return 0;
	if(bufer[1] != 0xd8)
		return 0;
	if(bufer[2] != 0xff)
		return 0;
	if((bufer[3] & 0xf0) != 0xe0)
		return 0;
	return 1;
}
 
int main(int argc, char *argv[])
{
	if(argc != 2){
		fprintf(stderr, "Usage: %s file\n", argv[0]);
		return 1;
	}
	FILE *card = fopen(argv[1], "r");
	if(!card){
		fprintf(stderr, "Error open card\n");
		return 1;
	}
	u_int8_t bufer[BUF_SIZE];
	char file_name[10];	// extra space
	FILE *jpg_file = NULL;
	int file_no = 0;
	while(fread(bufer, sizeof(bufer), 1, card)){
		if(is_jpg_header(bufer)){
			if(jpg_file){
				fclose(jpg_file);
			}
			sprintf(file_name, "%03d.jpg", file_no++);
			jpg_file = fopen(file_name, "w");
		}
		if(jpg_file){
			fwrite(bufer, sizeof(bufer), 1, jpg_file);
		}
	}

	fclose(jpg_file);
	fclose(card);
 
	return 0;
}
