# Week 4

## Filter and Filter2

A program that apply a filter to an image.

```
./filter [flag] input output
```

Flags:

* -g: grey scale
* -s: sepia	filter1 only
* -r: reverse
* -b: blur
* -e: edge	filter2 only

## Recover

Recover jpeg images from a storage, in this case is a .raw file.

```
./recover input_file
```

The program will search the header of a jpeg file in *inpit_file*, and will
create new jpeg files named 001.jpg-...-999.jpg.

The program assumes that jpeg files are one after the other. An file is finished
when the beginning of other is found.
Also the program doesn't look for the file size, it's using 512bytes chinks to
copy the files.
