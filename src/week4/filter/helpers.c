#include "helpers.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

// Convert image to grayscale
void grayscale(int height, int width, RGBTRIPLE image[height][width])
{
	int i,j;
	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			float avr = ((float)image[i][j].rgbtBlue + image[i][j].rgbtGreen +
					image[i][j].rgbtRed)/3;
			float rounded = roundf(avr);

			image[i][j].rgbtBlue = rounded;
			image[i][j].rgbtGreen = rounded;
			image[i][j].rgbtRed = rounded;
		}
	}
    return;
}

// Convert image to sepia
void sepia(int height, int width, RGBTRIPLE image[height][width])
{
	int i,j;
	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			// calculate sepia value
			float sepia_red = 0.393 * image[i][j].rgbtRed +
							0.769 * image[i][j].rgbtGreen +
							0.189 * image[i][j].rgbtBlue;
			float sepia_green = 0.349 * image[i][j].rgbtRed +
							0.686 * image[i][j].rgbtGreen +
							0.168 * image[i][j].rgbtBlue;
			float sepia_blue = 0.272 * image[i][j].rgbtRed +
							0.534 * image[i][j].rgbtGreen +
							0.131 * image[i][j].rgbtBlue;

			// round values
			sepia_red = roundf(sepia_red);
			sepia_green = roundf(sepia_green);
			sepia_blue = roundf(sepia_blue);

			// make them saller then 255
			if(sepia_red > 255)
				sepia_red = 255;
			if(sepia_green > 255)
				sepia_green = 255;
			if(sepia_blue > 255)
				sepia_blue = 255;

			// update colors
			image[i][j].rgbtBlue = sepia_blue;
			image[i][j].rgbtGreen = sepia_green;
			image[i][j].rgbtRed = sepia_red;
		}
	}
    return;
}

// Reflect image horizontally
void reflect(int height, int width, RGBTRIPLE image[height][width])
{
	int i,j;
	for(i=0; i<height; i++)
	{
		for(j=0; j<width/2; j++)
		{
			RGBTRIPLE aux = image[i][j];
			image[i][j] = image[i][width-1-j];
			image[i][width-1-j] = aux;
		}
	}
    return;
}

/* check if this position is valid */
int valid_pos(int x, int y, int height, int width)
{
	if(x < 0 || x >= height)
	   return 0;	
	if(y < 0 || y >= width)
		return 0;
	return 1;
}

/* calculcate the avrage of the pixels around a given pixel*/
RGBTRIPLE avr_around(int x, int y, int height, int width,
		RGBTRIPLE image[height][width])
{
	int nr_values = 0;
	float red_avr = 0, green_avr = 0, blue_avr = 0;
	int i,j;
	for(i = -1; i < 2; i++)
	{
		for(j = -1; j < 2; j++)
		{
			if(valid_pos(x+i,y+j,height,width))
			{
				nr_values++;
				red_avr += image[x+i][y+j].rgbtRed;
				green_avr += image[x+i][y+j].rgbtGreen;
				blue_avr += image[x+i][y+j].rgbtBlue;
			}
		}
	}

	red_avr = roundf(red_avr/nr_values);
	green_avr = roundf(green_avr/nr_values);
	blue_avr = roundf(blue_avr/nr_values);

	RGBTRIPLE avr = {
		.rgbtRed = red_avr,
		.rgbtGreen = green_avr,
		.rgbtBlue = blue_avr
	};


	return avr;
}

// Blur image
void blur(int height, int width, RGBTRIPLE image[height][width])
{
    RGBTRIPLE (*new_img)[width] = calloc(height, width * sizeof(RGBTRIPLE));
	int i,j;
	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			new_img[i][j] = avr_around(i,j, height, width, image);
		}
	}

	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			image[i][j] = new_img[i][j];
		}
	}

	free(new_img);
    return;
}
