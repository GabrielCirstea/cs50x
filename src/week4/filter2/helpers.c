#include "helpers.h"
#include <stdlib.h>
#include <math.h>

// Convert image to grayscale
void grayscale(int height, int width, RGBTRIPLE image[height][width])
{
	int i,j;
	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			float avr = ((float)image[i][j].rgbtBlue + image[i][j].rgbtGreen +
					image[i][j].rgbtRed)/3;
			float rounded = roundf(avr);

			image[i][j].rgbtBlue = rounded;
			image[i][j].rgbtGreen = rounded;
			image[i][j].rgbtRed = rounded;
		}
	}
    return;
}

// Reflect image horizontally
void reflect(int height, int width, RGBTRIPLE image[height][width])
{
	int i,j;
	for(i=0; i<height; i++)
	{
		for(j=0; j<width/2; j++)
		{
			RGBTRIPLE aux = image[i][j];
			image[i][j] = image[i][width-1-j];
			image[i][width-1-j] = aux;
		}
	}
    return;
}

/* check if this position is valid */
int valid_pos(int x, int y, int height, int width)
{
	if(x < 0 || x >= height)
	   return 0;	
	if(y < 0 || y >= width)
		return 0;
	return 1;
}

/* calculcate the avrage of the pixels around a given pixel*/
RGBTRIPLE avr_around(int x, int y, int height, int width,
		RGBTRIPLE image[height][width])
{
	int nr_values = 0;
	float red_avr = 0, green_avr = 0, blue_avr = 0;
	int i,j;
	for(i = -1; i < 2; i++)
	{
		for(j = -1; j < 2; j++)
		{
			if(valid_pos(x+i,y+j,height,width))
			{
				nr_values++;
				red_avr += image[x+i][y+j].rgbtRed;
				green_avr += image[x+i][y+j].rgbtGreen;
				blue_avr += image[x+i][y+j].rgbtBlue;
			}
		}
	}

	red_avr = roundf(red_avr/nr_values);
	green_avr = roundf(green_avr/nr_values);
	blue_avr = roundf(blue_avr/nr_values);

	RGBTRIPLE avr = {
		.rgbtRed = red_avr,
		.rgbtGreen = green_avr,
		.rgbtBlue = blue_avr
	};


	return avr;
}

// Blur image
void blur(int height, int width, RGBTRIPLE image[height][width])
{
    RGBTRIPLE (*new_img)[width] = calloc(height, width * sizeof(RGBTRIPLE));
	int i,j;
	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			new_img[i][j] = avr_around(i,j, height, width, image);
		}
	}

	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			image[i][j] = new_img[i][j];
		}
	}

	free(new_img);
    return;
}

/* calculate the pixel colors by applying the Gx or Gy matrix*/
int apply_red_matrix(int x, int y, int height, int width,
		int matrix[3][3], RGBTRIPLE image[height][width])
{
	int color_val = 0;
	int i,j;
	for(i = -1; i < 2; i++)
	{
		for(j = -1; j < 2; j++)
		{
			if(valid_pos(x+i,y+j,height,width))
			{
				color_val += image[x+i][y+j].rgbtRed * matrix[1+i][1+j];
			}
		}
	}
	return color_val;
}
int apply_green_matrix(int x, int y, int height, int width,
		int matrix[3][3], RGBTRIPLE image[height][width])
{
	int color_val = 0;
	int i,j;
	for(i = -1; i < 2; i++)
	{
		for(j = -1; j < 2; j++)
		{
			if(valid_pos(x+i,y+j,height,width))
			{
				color_val += image[x+i][y+j].rgbtGreen * matrix[1+i][1+j];
			}
		}
	}
	return color_val;
}
int apply_blue_matrix(int x, int y, int height, int width,
		int matrix[3][3], RGBTRIPLE image[height][width])
{
	int color_val = 0;
	int i,j;
	for(i = -1; i < 2; i++)
	{
		for(j = -1; j < 2; j++)
		{
			if(valid_pos(x+i,y+j,height,width))
			{
				color_val += image[x+i][y+j].rgbtBlue * matrix[1+i][1+j];
			}
		}
	}
	return color_val;
}

// Detect edges
void edges(int height, int width, RGBTRIPLE image[height][width])
{
	int i,j;
	int Gx[3][3] = { {-1, 0, 1},
					{-2, 0, 2},
					{-1, 0, 1}};

	int Gy[3][3] = {{-1, -2, -1},
					{0, 0, 0},
					{1, 2, 1}};

    RGBTRIPLE (*new_img)[width] = calloc(height, width * sizeof(RGBTRIPLE));

	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			int x_red = apply_red_matrix(i,j,height, width, Gx, image);
			int x_green = apply_green_matrix(i,j,height, width, Gx, image);
			int x_blue = apply_blue_matrix(i,j,height, width, Gx, image);

			int y_red = apply_red_matrix(i,j,height, width, Gy, image);
			int y_green = apply_green_matrix(i,j,height, width, Gy, image);
			int y_blue = apply_blue_matrix(i,j,height, width, Gy, image);

			float new_red = roundf(sqrt(x_red * x_red + y_red * y_red));
			float new_green = roundf(sqrt(x_green * x_green + y_green * y_green));
			float new_blue = roundf(sqrt(x_blue * x_blue + y_blue * y_blue));

			// make them saller then 255
			if(new_red > 255)
				new_red = 255;
			if(new_green > 255)
				new_green = 255;
			if(new_blue > 255)
				new_blue = 255;

			new_img[i][j].rgbtRed = new_red;
			new_img[i][j].rgbtGreen = new_green;
			new_img[i][j].rgbtBlue = new_blue;
		}
	}

	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			image[i][j] = new_img[i][j];
		}
	}
	free(new_img);

    return;
}
