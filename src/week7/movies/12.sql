select title from (
	-- movies in wich Johnny Depp stared
	select movies.id as id, title from movies
		join stars on movies.id = stars.movie_id
		join people on people.id = stars.person_id
		where lower(people.name) LIKE "%johnny depp%"
	) as t
	join stars on t.id = stars.movie_id
	join people on people.id = stars.person_id
	where lower(people.name) LIKE "%helena bonham carter%";

