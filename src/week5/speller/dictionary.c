// Implements a dictionary's functionality

#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "dictionary.h"

// Represents a node in a hash table
typedef struct node
{
    char word[LENGTH + 1];
    struct node *next;
}
node;

node *create_node(char *word)
{
	node *this = malloc(sizeof(*this));
	if(!this)
		return NULL;
	strcpy(this->word, word);
	this->next = NULL;

	return this;
}

// Number of buckets in hash table
const unsigned int N = 127;

// Hash table
node *table[127];
double word_count = 0;

// Returns true if word is in dictionary, else false
bool check(const char *word)
{
	unsigned int h = hash(word);
	node *it = table[h];
	while(it){
		if(strcasecmp(word, it->word) == 0)
			return true;
		it = it->next;
	}
    return false;
}

// Hashes word to a number
unsigned int hash(const char *word)
{
	int i;
	unsigned int sum_da = 0;
	for(i = 0; i[word]; i++){
		sum_da += i * tolower(word[i]);
	}

	return sum_da % N;
}

void insert_node(unsigned i, node* this)
{
	this->next = table[i];
	table[i] = this;
}

// Loads dictionary into memory, returning true if successful, else false
bool load(const char *dictionary)
{
	// open file
	FILE *dict = fopen(dictionary, "r");
	if(!dict){
		fprintf(stderr, "Can't open dictionary\n");
		return false;
	}
	char word[LENGTH+1];
	while(fscanf(dict, "%s", word) != EOF){
		node* new = create_node(word);
		if(!new){
			fprintf(stderr, "New nod error\n");
			return false;
		}

		unsigned int h = hash(word);

		insert_node(h, new);
		word_count++;
	}
	fclose(dict);
    return true;
}

// Returns number of words in dictionary if loaded, else 0 if not yet loaded
unsigned int size(void)
{
	return word_count;
}

// Unloads dictionary from memory, returning true if successful, else false
bool unload(void)
{
	int i;
	for(i=0; i<N; i++){
		node *it = table[i];
		node *next;
		while(it){
			next = it->next;
			free(it);
			it = next;
		}
		table[i] = NULL;
	}
    return true;
}
