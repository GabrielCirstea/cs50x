#!/bin/python3

from cs50 import get_float

def main():
    while True:
        number = get_float("Change owed: ")
        if number > 0:
            break

    number *= 100
    coins = 0
    while number >= 25:
        coins += 1
        number -= 25
    while number >= 10:
        coins += 1
        number -= 10
    while number >= 5:
        coins += 1
        number -= 5
    while number >= 1:
        coins += 1
        number -= 1

    print(coins)

main()
