#!/bin/python3

from cs50 import get_int

height = get_int("Height: ")
if height not in range(1,9):
    exit(1)

for h in range(height):
    print(" " * (height - 1 - h), end="")
    print("#" * (h + 1))
